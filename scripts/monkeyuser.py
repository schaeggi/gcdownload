#!/usr/bin/env python3

import argparse
import os
import pathlib
import sys
import urllib
from bs4 import BeautifulSoup as bs4
import colorama
from pynput import keyboard
from comicbaseclass import ComicBaseClass


# Download the "MonkeyUser Comic" by Cornel and Constantin

class Comic(ComicBaseClass):

    stop = False
    lastfile = False
        
    def __init__(self):
        # super().__init__()
        self.ID = "monkeyuser"
        self.TITLE = "MonkeyUser"
        self.AUTHOR = "Cornel and Constantin"
        self.BASE_URL = "https://www.monkeyuser.com"

    def download(self, number=10, destination=None):
        ''' get newest comic first and download "number" images backwards.'''
        print("\nDownloading MonkeyUser webcomic. Press ESC key to stop.\n")
        ComicBaseClass.download(self)
        number = 10 if number == None else number
        destination = pathlib.Path(self.download_folder) if destination == None else pathlib.Path(destination)
        self.create_path(destination)

        url = self.BASE_URL
        for i in range(number):
            if self.stop == True:
                if self.lastfile == True:
                    print("\nDownload stopped! No more files to download.\n")
                else:
                    print("\nDownload stopped!\n")
                break
            req = urllib.request.urlopen(url)
            content = bs4(req.read(), 'html.parser')
            try:
                prev = content.find('div', {'class':'prev'}).a
                prev_url = self.BASE_URL + prev['href']
                url = prev_url
            except AttributeError as e:
                self.stop = True
                self.lastfile = True
            try:
                img = content.find('div', {'class':'content'}).p.img
                img_url = img['src']
                filename = os.path.basename(urllib.parse.urlparse(img_url).path)
                self.save_file(img_url, destination.joinpath(filename))       
                yield
            except AttributeError as e:
                yield colorama.Fore.RED + "Error: No image file found <img>" + colorama.Style.RESET_ALL
            except TypeError as e:
                yield colorama.Fore.RED + "Error: Problem with the image source <img src='...'>" + colorama.Style.RESET_ALL
            except Exception as e:
                yield f"{colorama.Fore.RED}Error: Something is off ...{colorama.Style.RESET_ALL} {e}"


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='gcourt.py - downloader for MonkeyUser Webcomic.')
    parser.add_argument("-d", '--destination', help="Download path for images (optional)", metavar="path")
    parser.add_argument("-n", '--number', help="Number of images to download (starting with the newset one)", type=int, metavar="number", default=10)
    args = parser.parse_args()

    new = Comic()

    listener = keyboard.Listener(
        on_press=new.key_pressed)
    listener.start()

    
    if args.number or args.destination:
        for page in new.download(number=args.number, destination=args.destination):
            if page is not None:
                print(page)
    else:
        for page in new.download():
            if page is not None:
                print(page)

