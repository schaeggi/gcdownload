import pathlib
import re
import urllib
import urllib.request
import colorama
from bs4 import BeautifulSoup as bs4


main_url = "https://dord-orbit.tumblr.com/archive"
file_path = "./comics/dord/"

pages = (
    'https://dord-orbit.tumblr.com/post/133570024202',
    'https://dord-orbit.tumblr.com/post/133454231622',
    'https://dord-orbit.tumblr.com/post/133379129622',
    'https://dord-orbit.tumblr.com/post/133319335587',
    'https://dord-orbit.tumblr.com/post/133114708577',
    'https://dord-orbit.tumblr.com/post/133045761037',
    'https://dord-orbit.tumblr.com/post/132976491872',
    'https://dord-orbit.tumblr.com/post/132910989737',
    'https://dord-orbit.tumblr.com/post/132840783362',
    'https://dord-orbit.tumblr.com/post/132609359382',
    'https://dord-orbit.tumblr.com/post/132447142212',
    'https://dord-orbit.tumblr.com/post/132374969402',
    'https://dord-orbit.tumblr.com/post/132184601867',
    'https://dord-orbit.tumblr.com/post/132122300662',
    'https://dord-orbit.tumblr.com/post/132056693942',
    'https://dord-orbit.tumblr.com/post/131984741817',
    'https://dord-orbit.tumblr.com/post/131909907782',
    'https://dord-orbit.tumblr.com/post/131719393132',
    'https://dord-orbit.tumblr.com/post/131659413572',
    'https://dord-orbit.tumblr.com/post/131588825362',
    'https://dord-orbit.tumblr.com/post/131520031732',
    'https://dord-orbit.tumblr.com/post/131457349677',
    'https://dord-orbit.tumblr.com/post/131228518267',
    'https://dord-orbit.tumblr.com/post/131228508207',
    'https://dord-orbit.tumblr.com/post/131228498927',
    'https://dord-orbit.tumblr.com/post/131159466197',
    'https://dord-orbit.tumblr.com/post/131159458042',
    'https://dord-orbit.tumblr.com/post/131159448942',
    'https://dord-orbit.tumblr.com/post/131159440377',
    'https://dord-orbit.tumblr.com/post/131159431417',
)

def save_file(url, path, override=False):
    '''Save file from "url" to local "path". Use of absolute paths is strongly recommended!'''
    save_path = pathlib.Path(path)
    print(save_path)
    try:
        with urllib.request.urlopen(url) as response, open(save_path, 'wb') as file:
            file.write(response.read())
            print("Downloading " + save_path.name)
    except Exception as e:
        print("Something happened: %s" % e)


for i, page in enumerate(pages):  
    req = urllib.request.urlopen(page)
    content = bs4(req.read(), 'html.parser')
    image = content.find('div', {'class':'post'}).a.img
    print(image['src'])
    # print(page)
    save_file(image['src'], file_path + "dord" + str(i) + ".jpg")







