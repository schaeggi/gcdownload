import os
import pathlib
import urllib.request
import colorama
from pynput import keyboard




class ComicBaseClass():
    """Base class for webcomic downloader containing comprehensive properties and methods."""

    APP_FOLDER = pathlib.Path(__file__).resolve().parent.parent
    stop = 0

    def download(self):
        self.stopped = 0
    
    def save_file(self, url, path, override=False):
        '''Save file from "url" to local "path". Use of absolute paths is strongly recommended!'''
        path = pathlib.Path(path)
        # if not path.exists() or (path.exists() and override == True):
        if not path.exists() or override == True:
            try:
                with urllib.request.urlopen(url) as response, open(path, 'wb') as file:
                    file.write(response.read())
                    print("Downloading " + path.name)
            except Exception as e:
                print("Something happened: %s" % e)
        else:
            print(f"{colorama.Fore.YELLOW}File {path.name} already exists{colorama.Style.RESET_ALL}")

    def create_path(self, destination):
        destination = pathlib.Path(destination)
        if destination.exists():
            if not destination.is_dir():
                return
        else:
            os.makedirs(destination)

    @property
    def download_folder(self):
        return self.APP_FOLDER.joinpath("comics", self.ID)

    def next_file(self):
        filename = "test"
        # connect _next signal with this
        print("next file: %s" % filename)


    def key_pressed(self, key):
        '''check listener for ESC-keypress'''
        if key == keyboard.Key.esc:
            self.stop = True
            return False  # Stop listener


    def stop_download(self):
        self.stopped = 1
        print("download stopped")

    def set_download_folder(self):
        pass
