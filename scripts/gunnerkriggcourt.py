#!/usr/bin/env python3

import argparse
import os
import pathlib
import sys
import urllib
from bs4 import BeautifulSoup as bs4
import colorama
from pynput import keyboard
from comicbaseclass import ComicBaseClass


# Download the "GunnerkriggCourt Comic" by Tom Sidell

class Comic(ComicBaseClass):

    def __init__(self):
        self.ID = "gunnerkriggcourt"
        self.TITLE = "Gunnerkrigg Court"
        self.AUTHOR = "Tom Siddell"
        self.BASE_URL = "https://www.gunnerkrigg.com"
        self.URL_COUNT = "https://www.gunnerkrigg.com/comics"  # https://www.gunnerkrigg.com/comics/00002198.jpg
        self.FIRST = 1
        self.LAST = None
        self.EXTENSION = "jpg"
        
    def download(self, number=10, destination=None):
        ''' get newest comic first and download "number" images backwards.'''
        print("\nDownloading Gunnerkrigg Court webcomic. Press ESC key to stop.\n")
        number = 10 if number == None else number
        destination = pathlib.Path(self.download_folder) if destination == None else pathlib.Path(destination)
        self.create_path(destination)

        url = self.BASE_URL
        for i in range(number):
            if self.stop == True:
                print("\nDownload stopped!\n")
                break
            req = urllib.request.urlopen(url)
            content = bs4(req.read(), 'html.parser')

            try:
                prev = content.find('div', {'class':'extra'}).find('div', {'class':'nav'}).find('a', {'class':'left'})
                prev_url = self.BASE_URL + prev['href']
                url = prev_url
            except AttributeError as e:
                self.stop = True
                self.lastfile = True
            try:
                img = content.find('img', {'class':'comic_image'})
                img_url = self.BASE_URL + img['src']
                filename = os.path.basename(urllib.parse.urlparse(img_url).path)
                self.save_file(img_url, destination.joinpath(filename))
                yield filename
            except AttributeError as e:
                yield colorama.Fore.RED + "Error: No image file found (Attribute Error)." + colorama.Style.RESET_ALL
            except TypeError as e:
                yield colorama.Fore.RED + "Error: Problem with the image source (Type Error)." + colorama.Style.RESET_ALL
            except Exception as e:
                yield f"{colorama.Fore.RED}Error: Something is off ...{colorama.Style.RESET_ALL} {e}"


    def loader(self, first=None, last=None, destination=None):
        ''' download comics from FIRST fo LAST'''
        first = self.FIRST if first == None else first
        last = self.LAST if last == None else last
        destination = self.download_folder if destination == None else destination

        print(f'first: {first}')
        print(f'last: {last}')
        print(f'destination: {destination}')
        print(os.getcwd())

        i = first
        if destination.exists():
            print("exists")
            if not destination.is_dir():
                print("cant create output folder")
                return
        else:
            os.makedirs(destination)

        while True:
            try:
                print("Trying")
                file = str(i).zfill(8) + "." + self.EXTENSION
                download_path = destination.joinpath(file)
                if self.URL_COUNT[-1] != "/":
                    request_url = self.URL_COUNT + "/" + file
                else:
                    request_url = self.URL_COUNT + file

                self.save_file(request_url, download_path)

                # if not download_path.exists():
                #     with urllib.request.urlopen(request_url) as response, open(download_path, 'wb') as out_file:
                #         out_file.write(response.read())

                # else:
                #     print(file + " already exists")

                if i == last:
                    print("last")
                    break
                else:
                    print("next")
                    i+=1

            except Exception as e:
                print("except %s" % e)
                break
            
        print("All completed")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='gcourt.py - downloader for Gunnerkrigg Court Webcomic.')
    parser.add_argument("-d", '--destination', help="Download path for images (optional)", metavar="path")
    parser.add_argument("-n", '--number', help="Number of images to download (starting with the newset one)", type=int, metavar="number", default=10)
    args = parser.parse_args()

    new = Comic()
    
    listener = keyboard.Listener(
    on_press=new.key_pressed)
    listener.start()

    if args.number or args.destination:
        for page in new.download(number=args.number, destination=args.destination):
            pass
    else:
        for page in new.download():
            pass