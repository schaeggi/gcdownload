#!/usr/bin/env python3

import importlib
import pathlib
import urllib
import sys
from PyQt5 import QtCore, QtWidgets
from main_window import Ui_MainWindow
from about_window import Ui_Dialog as About_Dialog
from info_window import Ui_Dialog as Info_Dialog
from pylib import styles, get_comics_from_toml, get_version_from_toml, logger, _translate


# dictionary of available comics (value) and its package name (key)
comics_from_toml = get_comics_from_toml()
if comics_from_toml is False:
    logger.critical("[basclass>main] The comics.toml file is missing!")
    raise FileNotFoundError("Something very bad happened. The comics.toml file is missing!")


# start button clicked
def start():
    ui.start_button.setEnabled(False)
    ui.stop_button.setEnabled(True)
    
    selected_comic = get_selection()

    # create thread
    comic = importlib.import_module('loaders.%s' % selected_comic)

    ui.loader = comic.Comic()
    ui.loader.selection = comics_from_toml[selected_comic]

    # Add options to comic data (ui.loader.selection)
    ui.loader.selection['override'] = ui.options_overwrite_box.isChecked()
    try:
        ui.loader.selection['path'] = ui.path
    except Exception:
        ui.loader.selection['path'] = None
    if ui.radioLatest.isChecked():
        ui.loader.selection['pages'] = ui.spinLatest.value()
    elif ui.radioAll.isChecked():
        ui.loader.selection['pages'] = None

    # connect signals
    ui.loader.messageSignal.connect(send_message)
    ui.loader.doneSignal.connect(loader_done)
    ui.loader.stopSignal.connect(loader_stopped)

    # run thread
    ui.loader.start()


@QtCore.pyqtSlot(str)
def send_message(message):  # display message on textview
    ui.progress_text.moveCursor(11)
    ui.progress_text.insertHtml(message+"<br>")


@QtCore.pyqtSlot()
def loader_stopped():
    ui.progress_text.moveCursor(11)
    ui.progress_text.insertHtml(_translate('main', '<span style=" color: %s;">Stopped</span><br><br>' % styles.COL_RED))
    ui.progress_text.moveCursor(11)
    ui.start_button.setEnabled(True)


@QtCore.pyqtSlot()
def loader_done():
    ui.progress_text.moveCursor(11)
    ui.progress_text.insertHtml(_translate('main', "<strong>Completed</strong><br><br>"))
    ui.progress_text.moveCursor(11)
    ui.start_button.setEnabled(True)
    ui.stop_button.setEnabled(False)


def stop():  # stop button clicked
    ui.stop_button.setEnabled(False)
    ui.loader.stop = True


def info_button_clicked():  # comic info button pressed
    
    def filter_url(url):
        new_url = urllib.parse.urlsplit(url)
        return new_url[1]

    Dialog = QtWidgets.QDialog()
    info = Info_Dialog()
    info.setupUi(Dialog)
    selected_comic = get_selection()

    if comics_from_toml[selected_comic]['description'] == "" or comics_from_toml[selected_comic]['description'] is None:
        comics_from_toml[selected_comic]['description'] = _translate('main', "There is no description for this comic yet.")

    info.title_label.setText(comics_from_toml[selected_comic]['name'])
    info.link_label.setText('<a href="' + comics_from_toml[selected_comic]['url_main'] + '">' + filter_url(comics_from_toml[selected_comic]['url_main']) + '</a>')
    info.link_label.setOpenExternalLinks(True)
    info.artist_label.setText("by " + comics_from_toml[selected_comic]['author'])

    # try to load a comic description from 'comicid.htm' file, if none exists, use description from comics.toml
    filepath = pathlib.Path("app").joinpath('loaders', comics_from_toml[selected_comic]['id'] + ".htm")
    if filepath.exists():
        info.description_box.setSource(QtCore.QUrl.fromLocalFile(str(filepath)))
    else:
        info.description_box.setPlainText(comics_from_toml[selected_comic]['description'])

    response = Dialog.exec_()


def clear_button_clicked():  # clear the download folder selection
    ui.path = None
    ui.download_folder_box.setText("...")


def about_app():  # about from application menu selected
    Dialog = QtWidgets.QDialog()
    about = About_Dialog()
    about.setupUi(Dialog)
    about.title_label.setText(get_version_from_toml()[0])
    about.version_label.setText(_translate('main', "Version ") + get_version_from_toml()[1])
    response = Dialog.exec_()



def quit_app():  # quit from application menu selected or closed with x
    app.quit()


def get_selection():  # get classname / key from combobox selection
    choice = ui.select_comic_box.currentText()
    for k, v in comics_from_toml.items():
        if v['name'] == choice:
            return k


# connect ui elements (additional method for Ui_MainWindow)
def connectUi(self):
    self.actionAbout.triggered.connect(about_app)
    self.actionQuit.triggered.connect(quit_app)
    self.start_button.clicked.connect(start)
    self.stop_button.clicked.connect(stop)
    self.download_folder_box.clicked.connect(folder_selection)
    self.info_button.clicked.connect(info_button_clicked)
    self.clear_button.clicked.connect(clear_button_clicked)

    for k, v in comics_from_toml.items():
        self.select_comic_box.addItem(v['name'])


def folder_selection():
    file_dialog = QtWidgets.QFileDialog(MainWindow)
    ui.path = file_dialog.getExistingDirectory(caption=_translate('main', "Choose download location"))
    ui.download_folder_box.setText("..." + ui.path[-25:])


# add method to Ui_MainWindow
setattr(Ui_MainWindow, "connectUi", connectUi)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    app.aboutToQuit.connect(quit_app)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    ui.connectUi()
    MainWindow.setWindowTitle("gcdownload")  # show title and version from toml
    MainWindow.show()
    sys.exit(app.exec_())
