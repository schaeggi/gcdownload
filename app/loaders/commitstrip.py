import urllib.request
from bs4 import BeautifulSoup as bs4
from loaders import baseclass


# 1. go to url
# 2. get first <div class="excerpt"> and follow href to the latest comic
# 3. get image from <div class="entry-content">
# 4. follow <span class="nav-previous"> to the previous page a href
# 5. goto 3

# TODO: fix problem with "unusual" characters


class Comic(baseclass.Loader):

    @property
    def BASE_URL(self):
        req = urllib.request.urlopen(self.selection['url_download'])
        content = bs4(req.read(), 'html.parser')
        url = content.find('div', {'class': 'excerpt'}).section.a
        return url['href']

    def get_image(self, content):
        img = content.find('div', {'class': 'entry-content'}).p.img
        return img['src']

    def get_previous(self, content):
        prev = content.find('span', {'class': 'nav-previous'}).a
        return self.BASE_URL + prev['href']
