import os
import pathlib
import urllib
import urllib.request
from pylib import styles, codes, logger, download_type, _translate
from bs4 import BeautifulSoup as bs4
from PyQt5 import QtCore



class Loader(QtCore.QThread):

    doneSignal = QtCore.pyqtSignal()
    stopSignal = QtCore.pyqtSignal()
    messageSignal = QtCore.pyqtSignal(str)
    downloadType = download_type.SINGLE
    selection = {}
    APP_FOLDER = pathlib.Path(__file__).resolve().parent.parent.parent

    @property 
    def BASE_URL(self):
        return self.selection['url_download']

    @property
    def default_path(self):
        return self.APP_FOLDER.joinpath('comics', self.selection['id'])


    def run(self):
        self.stop = False
        self.last_page = False
        counter = 0
        url = self.BASE_URL

        self.messageSignal.emit("<strong>Starting download of %s</strong>" % self.selection['name'])
        if self.selection['path'] is None or self.selection['path'] == "":
            self.messageSignal.emit(_translate('baseclass', "Warning: No path selected, using default_path."))
            self.selection['path'] = self.default_path
            self.create_path(self.selection['path'])

        while (self.selection['pages'] is None) or (counter < self.selection['pages']):

            # main download loop -> for multiple pages, each containing one comic page
            if self.downloadType == download_type.SINGLE:
                req = urllib.request.urlopen(url)
                content = bs4(req.read(), 'html.parser')

                try:
                    prev_url = self.get_previous(content)
                    prev_url = prev_url.replace(" ", "%20")
                except Exception: # (AttributeError, TypeError, KeyError):  # no prev_url available / KeyError: 'href'
                    self.last_page = True

                try: 
                    image_url = self.get_image(content)
                    image_url = image_url.replace(" ", "%20")
                except Exception as e:
                    self.messageSignal.emit(_translate('baseclass', '<span style=" color: %s;">No image found. Going to next page.</span>' % (styles.COL_RED)))
                    logger.info("[basclass>run] " + str(e))  # TODO: add img url
                    image_url = None

                if image_url is not None:
                    filename = os.path.basename(urllib.parse.urlparse(image_url).path)
                    result = self.save_file(image_url, pathlib.Path(self.selection['path']).joinpath(filename), self.selection['override'])                    

                if self.stop is True:
                    self.stopSignal.emit()
                    break

                if self.last_page == True:
                    self.doneSignal.emit()
                    break

                url = prev_url
                counter += 1


            # main download loop -> for something completely custom.
            elif self.downloadType == download_type.CUSTOM:
                req = urllib.request.urlopen(url)
                content = bs4(req.read(), 'html.parser')
                self.messageSignal.emit(_translate('baseclass', "<br><b>Collecting files ...</b>"))
                self.custom_pages = self.get_comic_list(content)
                
                if self.stop is not True:

                    self.messageSignal.emit(_translate('baseclass', "<br><b>Saving files ...</b>"))
                    
                    for i, each in enumerate(self.custom_pages):
                        
                        result = self.save_file(each[0], each[1], self.selection['override'])
                                               
                        if self.stop is True:
                            self.stopSignal.emit()
                            break

                if self.stop is not True:

                    self.messageSignal.emit("") # newline?
                    self.doneSignal.emit()
                
                break


        else:
            # loop completed without breaking
            self.doneSignal.emit()
            return


    def save_file(self, url, path, override=False):
        '''Save file from "url" to local "path". Use of absolute paths is strongly recommended!
        'path' can be a string or path object, 'url' should be a string.
        'override' (boolean) to overwrite existing files is optional. Default value is 'False' '''


        path = pathlib.Path(path)
        filename = os.path.basename(urllib.parse.urlparse(url).path)

        # filename = urllib.parse.quote(filename, safe=':/')
        url = urllib.parse.quote(url, safe=':/')  # encode problematic characters in url

        if not path.exists() or override is True:

            try:
                with urllib.request.urlopen(url) as response, open(path, 'wb') as file:
                    file.write(response.read())
                self.messageSignal.emit(_translate('baseclass', "%s ... done" % filename))
                return codes.FILE_SAVED

            
            except Exception as e:
                logger.error("[baseclass>save_file] File could not be saved: %s" % e)
                self.messageSignal.emit(_translate('baseclass', '<span style=" color: %s;">Something bad happened: File %s could not be created.</span>' % (styles.COL_RED, filename)))
                return codes.FILE_ERROR

        else:
            self.messageSignal.emit(_translate('baseclass', '<span style=" color: %s;">%s already exists</span>' % (styles.COL_ORANGE, filename)))
            return codes.FILE_EXISTS


    def create_path(self, destination):
        ''' check if a folder exists, and if not, create it. 'destination' can be a string or Path object '''

        destination = pathlib.Path(destination)
        if destination.exists():
            if not destination.is_dir():
                self.messageSignal.emit(_translate('baseclass', "Can not create path. File already exists."))
                logger.info(f"[baseclass>create_path] Can not create path. File already exists: {destination}")
                return False
            else:
                self.messageSignal.emit(_translate('baseclass', "Path already exists: %s" % destination))
                logger.info(f"[baseclass>create_path] Path already exists: {destination}")
                return True
        else:
            try:
                os.makedirs(destination)
                return True
            except Exception as e:
                self.messageSignal.emit(_translate('baseclass', "Error creating path %s" % destination))
                logger.info("[baseclass>create_path] Error creating path %s" % destination)
                return False


    def clean_url(self, url):
        '''clean urls that use requests:
        This: www.gunnerkrigg.com/extracomics/comic.php?c=Traveller&p=40
        returns: www.gunnerkrigg.com/extracomics/comic.php
        To get rid of the last / part use:
        > clean_url(BASE_URL).rsplit("/",1)
        You NEED to add "http://" or "https://" at the front after cleaning! '''

        split_url = urllib.parse.urlsplit(url)
        return str(split_url[1]+split_url[2])


    def get_comic_list(self, content):
        comic_list = [("url", "path"), ("url", "path"), ("url", "path")]
        return comic_list
