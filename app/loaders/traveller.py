from loaders import baseclass


class Comic(baseclass.Loader):


    def get_image(self, content):
        img = content.find('a', {'class': 'comic_image'}).img
        newrl = self.clean_url(self.BASE_URL).rsplit("/", 1)
        return "https://" + newrl[0] + "/" + img['src']

    def get_previous(self, content):
        prev = content.find('div', {'class': 'under'}).find('div', {'class': 'left'}).a
        return "https://" + self.clean_url(self.BASE_URL) + prev['href']
