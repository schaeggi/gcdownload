import os
import pathlib
import urllib.request
from bs4 import BeautifulSoup as bs4
from loaders import baseclass
from pylib import styles, logger, download_type



class Comic(baseclass.Loader):
    
    downloadType = download_type.CUSTOM
    comic_list = []
    pages = ( # all 30 pages with dord comics
        'https://dord-orbit.tumblr.com/post/133570024202',
        'https://dord-orbit.tumblr.com/post/133454231622',
        'https://dord-orbit.tumblr.com/post/133379129622',
        'https://dord-orbit.tumblr.com/post/133319335587',
        'https://dord-orbit.tumblr.com/post/133114708577',
        'https://dord-orbit.tumblr.com/post/133045761037',
        'https://dord-orbit.tumblr.com/post/132976491872',
        'https://dord-orbit.tumblr.com/post/132910989737',
        'https://dord-orbit.tumblr.com/post/132840783362',
        'https://dord-orbit.tumblr.com/post/132609359382',
        'https://dord-orbit.tumblr.com/post/132447142212',
        'https://dord-orbit.tumblr.com/post/132374969402',
        'https://dord-orbit.tumblr.com/post/132184601867',
        'https://dord-orbit.tumblr.com/post/132122300662',
        'https://dord-orbit.tumblr.com/post/132056693942',
        'https://dord-orbit.tumblr.com/post/131984741817',
        'https://dord-orbit.tumblr.com/post/131909907782',
        'https://dord-orbit.tumblr.com/post/131719393132',
        'https://dord-orbit.tumblr.com/post/131659413572',
        'https://dord-orbit.tumblr.com/post/131588825362',
        'https://dord-orbit.tumblr.com/post/131520031732',
        'https://dord-orbit.tumblr.com/post/131457349677',
        'https://dord-orbit.tumblr.com/post/131228518267',
        'https://dord-orbit.tumblr.com/post/131228508207',
        'https://dord-orbit.tumblr.com/post/131228498927',
        'https://dord-orbit.tumblr.com/post/131159466197',
        'https://dord-orbit.tumblr.com/post/131159458042',
        'https://dord-orbit.tumblr.com/post/131159448942',
        'https://dord-orbit.tumblr.com/post/131159440377',
        'https://dord-orbit.tumblr.com/post/131159431417',
    )

    
    def get_comic_list(self, content):

        for i, page in enumerate(self.pages):
            if self.selection['pages'] is not None and i >= self.selection['pages']:
                break
            if self.stop is True:
                self.stopSignal.emit()
                break
            req = urllib.request.urlopen(page)
            content = bs4(req.read(), 'html.parser')
            image = content.find('div', {'class':'post'}).a.img
            filename = os.path.basename(urllib.parse.urlparse(image['src']).path)
            self.comic_list.append([image['src'], pathlib.Path(self.selection['path']).joinpath("dord" + filename[6:]), "dord" + filename[6:]])
            self.messageSignal.emit("%s ..." % filename)

        return self.comic_list
