from loaders import baseclass




class Comic(baseclass.Loader):

    def get_image(self, content):
        img = content.find('a', {'class':'comic_image'}).img
        newrl = self.clean_url(self.BASE_URL).rsplit("/",1)
        url = "https://" + newrl[0] + "/" + img['src']
        return url  # .replace(" ", "%20")

    def get_previous(self, content):
        prev = content.find('div', {'class':'under'}).find('div', {'class':'left'}).a
        url = "https://" + self.clean_url(self.BASE_URL)+prev['href']
        return url  # .replace(" ", "%20")
