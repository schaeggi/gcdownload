from loaders import baseclass



class Comic(baseclass.Loader):

    def get_image(self, content):
        img = content.find('div', {'class': 'content'}).p.img
        return img['src']

    def get_previous(self, content):
        prev = content.find('div', {'class': 'prev'}).a
        return self.BASE_URL + prev['href']
