from loaders import baseclass
from pylib import logger


class Comic(baseclass.Loader):

    def get_image(self, content):
        img = content.find('div', {'id':'comic'}).img
        return 'https:' + img['src']

    def get_previous(self, content):
        prev = content.find('ul', {'class': 'comicNav'}).find('a', {'rel': 'prev'})
        if prev['href'] == "#":
            self.last_page = true
            return False
        return self.BASE_URL + prev['href']
