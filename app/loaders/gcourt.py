from loaders import baseclass


class Comic(baseclass.Loader):

    def get_image(self, content):
        img = content.find('img', {'class': 'comic_image'})
        return self.BASE_URL + img['src']

    def get_previous(self, content):
        prev = content.find('div', {'class': 'extra'}).find('div', {'class': 'nav'}).find('a', {'class': 'left'})
        return self.BASE_URL + prev['href']
