import logging
import pathlib
from PyQt5 import QtCore
import toml

_translate = QtCore.QCoreApplication.translate

LOG_FORMAT = "%(asctime)s - %(levelname)s - %(message)s"
logfile = pathlib.Path(__file__).resolve().parent.parent.joinpath("app", "logs", "app.log")
logfilepath = pathlib.Path(logfile)
if not logfilepath.exists():
    logfilepath.parent.mkdir(parents=True)
logging.basicConfig(filename=logfile, level=logging.INFO, format=LOG_FORMAT, filemode='w')
logger = logging.getLogger()


def get_comics_from_toml():
    '''Get available comic data from comics.toml file'''
    filename = pathlib.Path("app").joinpath("comics.toml")
    try:
        with open(filename, 'r') as file:
            data = toml.load(file)
        return data
    except FileNotFoundError:
        logger.warning(f'[pylib>get_comics_from_toml] file "{filename}" not found')
        return False
    except Exception as e:
        logger.error(f'[pylib>get_comics_from_toml] Something bad happened while loading {filename}:\n{e}')
        return False


def get_version_from_toml():
    '''return a tuple containing application name(0) and version(1) from pyproject.toml file'''
    print()
    try:
        pyproject_path = pathlib.Path(__file__).resolve().parent.parent.joinpath("pyproject.toml")
        with open(pyproject_path, "r") as file:
            data = toml.load(file)
        return (data['tool']['poetry']['name'], data['tool']['poetry']['version'])
    except FileNotFoundError:
        logger.warning(f"[pylib>get_version_from_toml] pyproject.toml file is missing.")
    except Exception as e:
        logger.error(f"[pylib>get_version_from_toml] Error opening pyproject.toml file: {e}")
        return False


class Codes():
    '''custom return codes'''
    FILE_SAVED = 0
    FILE_EXISTS = 1
    FILE_ERROR = 2



class Styles():
    ''' style information for html formatting text ui output'''
    COL_ORANGE = "#f57900"
    COL_RED = "#cc0000"



class DownloadTypes():
    DEFAULT = 0  # one comic per page, default
    SINGLE = 0  # one comic per page, default
    MULTIPLE = 1  # multiple comics per page
    CUSTOM = 5  # something completely different

 

codes = Codes()

styles = Styles()

download_type = DownloadTypes()
