#!/usr/bin/env python3


import setuptools


# get long description from readme file
long_description = "Not available."
try:
    with open("README.md", "r") as file:
        long_description = file.read()
except Exception:
    print("Error reading README.dm file.")

# get version number from pyproject.toml file
appversion = "0.0.0"
try:
    with open("pyproject.toml", "r") as file:
        for line in file:
            if line.lower().startswith("version"):
                version = line.split("=")
                appversion = version[1].strip(" \"\n")
                break
except Exception:
    print("Error reading pyproject.toml file.")

# run setup
setuptools.setup(
    name="gcdownload",
    version=appversion,
    license="MIT",
    author="Schäggi Popäggi",
    description="A webcomic download helper",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/fryk/gcdownload",
    packages=setuptools.find_packages(exclude=['tests', 'tests.*']),
    install_requires=['toml>=^0.10.0', 'beautifulsoup4>=4.8', 'pyQt5>=5.13'],  # FIXME: error installing pyqt5 with pip
    extras_require={"dev": ["flake8>=3.7"]},
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
