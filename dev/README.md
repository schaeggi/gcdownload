# Development files

**about_window.ui:**

Qt 5 Designer Ui file for the about window.

**create_toml.py:**

Create the comics.toml file from a python dictionary.

**info_window.ui:**

Qt 5 Designer Ui file for the comic info window.

**logo.png:**

Application logo as png.

**logo.svg:**

Application logo as svg.

**main_window.ui:**

Qt 5 Designer Ui file for the main window.

**pyqtupdate.sh:**

(Re)Compile all *.ui files and the resources.qrc file.

**resources.qrc:**

Qt 5 Designer Resource file, contains the app logo.

**scrape_test.py:**

Can be used to quickly test scraping a new webcomic without starting from scratch.