#! /bin/bash

# create python files from ui files

echo "Recompiling main_window.ui"
pyuic5 -x dev/main_window.ui -o app/main_window.py

echo "Recompiling about_window.ui"
pyuic5 -x dev/about_window.ui -o app/about_window.py

echo "Recompiling info_window.ui"
pyuic5 -x dev/info_window.ui -o app/info_window.py

echo "Recompiling resources.qrc"
pyrcc5 dev/resources.qrc -o app/resources_rc.py