import os
import pathlib
import sys
import urllib
import urllib.request  # needed in addition to urllib only
from bs4 import BeautifulSoup as bs4


# Additional tools from Loader class:

def clean_url(url):  # used for urls with requests
    split_url = urllib.parse.urlsplit(url)
    return str(split_url[1]+split_url[2])


# Scrape testing


BASE_URL = "https://www.xkcd.com"

url = BASE_URL


def get_image(content):
    # Example: img = content.find('a', {'class':'comic_image'}).img
    img = content.find('div', {'id':'comic'}).img
    #newrl = clean_url(BASE_URL).rsplit("/",1)
    return 'https:' + img['src'] # newrl[0] + "/" + img['src']


def get_previous(content):
    prev = content.find('ul', {'class':'comicNav'}).find('a', {'rel': 'prev'})
    return BASE_URL + prev['href']
#     # Example: prev = content.find('div', {'class':'under'}).find('div', {'class':'left'}).a
#     prev = content.find('div', {'class':'under'}).find('div', {'class':'left'}).a
#     url = clean_url(BASE_URL)+prev['href']
#     return url.replace(" ", "%20")


# Generate output

req = urllib.request.urlopen(url)
content = bs4(req.read(), 'html.parser')

image_url = get_image(content)
print(f'image_url: {image_url}')

prev_url = get_previous(content)
print(f'prev_url: {prev_url}')
