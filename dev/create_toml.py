import argparse
import toml

default_filename = "../app/comics.toml"

collection = {
    'commitstrip': {
        'id': 'commitstrip',
        'name': "CommitStrip",
        'author': "Thomas Gx, translated into English by Mark Nightingale.",
        'description': "One day, a bunch of developers met an illustrator. The result? CommitStrip – a daily strip recounting funny anecdotes of life as a coder mixed up with a dash of topical tech news. It’s the kind of thing we think developers will like to read between two cups of coffee. Whether today’s strip is about coding, life in a web agency, our teammates, customers, topical news or even cute cats, we hope you’ll enjoy it as much as we enjoyed writing it.",
        'url_main': "http://www.commitstrip.com",
        'url_download': "http://www.commitstrip.com/en/",
        'license': { 
            'name': None,  
            'link': 'http://www.commitstrip.com/en/about/',
        },
        # 'path': "/path/where/tosave/thefiles/"
    },
    'monkeyuser': {
        'id': 'monkeyuser',
        'name': "MonkeyUser",
        'author': "Cornel and Constantin",
        'description': "MonkeyUser was created out of a desire to bring joy to people working in IT by making life a little less boring.",
        'url_main': "https://www.monkeyuser.com",
        'url_download': "https://www.monkeyuser.com",
        'license': { 
            'name': None,  
            'link': 'https://www.monkeyuser.com/about',
        },
        # 'path': "/path/where/tosave/thefiles/"
    },
    'xkcd': {
        'id': 'xkcd',
        'name': "xkcd",
        'author': "Randall Munroe",
        'description': "A webcomic of romance, sarcasm, math, and language.",
        'url_main': "https://xkcd.com/",
        'url_download': "https://xkcd.com/",
        'license': { 
            'name': "CC BY-NC 2.5",  
            'link': 'https://xkcd.com/about/',
        },
    },
    'dord': {
        'id': 'dord',
        'name': "Derelict Orbital Reflector Devices",
        'author': "?",
        'description': "A story about two poetic orbital reflector devices and their makers, that are long gone.",
        'url_main': "https://dord-orbit.tumblr.com/archive",
        'url_download': "https://dord-orbit.tumblr.com",
        'license': { 
            'name': None,  
            'link': 'https://dord-orbit.tumblr.com',
        },
    },
    'gcourt': {
        'id': 'gcourt',
        'name': "Gunnerkrigg Court",
        'author': "Tom Siddell",
        'description': "",
        'url_main': "https://www.gunnerkrigg.com",
        'url_download': "https://www.gunnerkrigg.com",
        'license': { 
            'name': None,  
            'link': 'https://www.gunnerkrigg.com',
        },
    },
    'traveller': {
        'id': 'traveller',
        'name': "Traveller",
        'author': "Tom Siddell",
        'description': "",
        'url_main': "https://www.gunnerkrigg.com/extracomics/comic.php?c=Traveller",
        'url_download': "https://www.gunnerkrigg.com/extracomics/comic.php?c=Traveller&p=41",
        'license': { 
            'name': None,  
            'link': 'https://www.gunnerkrigg.com',
        },
    },
    'annieforest1': {
        'id': 'annieforest1',
        'name': "Annie in the forest (part 1)",
        'author': "Tom Siddell",
        'description': "",
        'url_main': "https://www.gunnerkrigg.com/extracomics/comic.php?c=Annie%20in%20the%20Forest%20Part%201",
        'url_download': "https://www.gunnerkrigg.com/extracomics/comic.php?c=Annie%20in%20the%20Forest%20Part%201&p=33",
        'license': { 
            'name': None,  
            'link': 'https://www.gunnerkrigg.com',
        },
    },
    'annieforest2': {
        'id': 'annieforest2',
        'name': "Annie in the forest (part 2)",
        'author': "Tom Siddell",
        'description': "",
        'url_main': "https://www.gunnerkrigg.com/extracomics/comic.php?c=Annie%20in%20the%20Forest%20Part%202",
        'url_download': "https://www.gunnerkrigg.com/extracomics/comic.php?c=Annie%20in%20the%20Forest%20Part%202&p=33",
        'license': { 
            'name': None,  
            'link': 'https://www.gunnerkrigg.com',
        },
    },
}

def update_collection(filename=default_filename):
    with open(filename, 'w') as file:
        data = toml.dumps(collection)
        file.write(data)

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='get filepath of comics.toml file')
    parser.add_argument("file", help="path to comics.toml")
    args = parser.parse_args()
    update_collection(args.file)