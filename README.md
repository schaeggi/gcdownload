# gcdownload


## About

A simple webcomic download helper. Make a local backup of your favorite webcomic.

Released under MIT license

Website: gitlab.com/fryk/gcdownload

Please visit the webcomic / artists websites from time to time and think about donating or how to support them otherwise.


## Supported webcomics

- MonkeyUser (https://www.monkeyuser.com)
- xkcd (https://xkcd.com)
- Derelict Orbital Reflector Devices (https://dord-orbit.tumblr.com)
- Gunnerkrigg Court (https://www.gunnerkrigg.com)
- Traveller (https://www.gunnerkrigg.com/extracomics)
- Annie in the forest (https://www.gunnerkrigg.com/extracomics)


## Installation instructions on Linux

Dependencies:

- Python 3.6 or newer, www.python.org
- Poetry, www.python-poetry.org


Installation:
    
    git clone https://gitlab.com/fryk/gcdownload.git
    cd *gcdownload_folder*
    poetry install


Run the app:

    sh ./launcher


## Add new comics

Yes, with a little bit of python3 / html knowledge its not that hard to add most other webcomics. This is how it can be done.

Two steps are needed to add a new comic to the gcdownload app:
- add *mycomic* to '/app/comics.toml'
- add custom python class in '/app/loaders/*mycomic*.py'
- optional: add '/app/loaders/*mycomic*.htm' containing a more extensive description


### (1) comics.toml configuration file

    [mycomic]
    id = "mycomic"  # same as [mycomic]
    name = "My Comic"  # full name of the webcomic
    author = "Author Name"
    description = "A webcomic about a webcomic."
    url_main = "https://www.mycomic.com"  # main url of the comic (short url)
    url_download = "https://www.mycomic.com&page=new"  # url of the latest webcomic image
    [mycomic.license]
    name = "CC BY-NC 2.5"
    link = "https://link-to-license/or-more-info-about.it"


### (2.1) *mycomic*.py webscraping file from multiple webpages containing one comic image each

*mycomic*.py needs 2 methods

    from loaders import baseclass
    
    class Comic(baseclass.Loader):

        def get_image(self, content):
            # img = string of the comic image file
            return img

        def get_previous(self, content):
            # prev = string of the previos comic page
            return prev

If you are new to this, just take a look at the existing files in the 'loaders' folder like monkeyuser.py and gcourt.py.

### (2.2) *mycomic*.py webscraping file for comics where the first way does not work



*mycomic*.py needs 1 method

    from loaders import baseclass
    
    class Comic(baseclass.Loader):

        def get_comic_list(self, content):
            # comic list = [(image_url_1, local_save_path_1), (image_url_2, local_save_path_2), (image_url_3, local_save_path_3), ...]
            return comic_list

Example: see app/loaders/dord.py


## Known issues
